import java.util.*;

public class Maximum {
	
		static int maxCrossingSum(int arr[], int l, int m,int h){
			// Include elements on left of mid.
			int sum = 0;
			int left_sum = Integer.MIN_VALUE;
			for (int i = m; i >= l; i--) {
				sum = sum + arr[i];
				if (sum > left_sum)
					left_sum = sum;
			}

			// Include elements on right of mid
			sum = 0;
			int right_sum = Integer.MIN_VALUE;
			for (int i = m; i <= h; i++) {
				sum = sum + arr[i];
				if (sum > right_sum)
					right_sum = sum;
			}
			return Math.max(left_sum + right_sum - arr[m],Math.max(left_sum, right_sum));
		}
		static int maxSubArraySum(int arr[], int l, int h)
		{
			//Invalid Range: low is greater than high
			if (l > h)
				return Integer.MIN_VALUE;
			// Base Case: Only one element
			if (l == h)
				return arr[l];

			// Find middle point
			int m = (l + h) / 2;
			return Math.max(
				Math.max(maxSubArraySum(arr, l, m-1),
						maxSubArraySum(arr, m + 1, h)),
				maxCrossingSum(arr, l, m, h));
		}
		public static void main(String[] args)
		{
			int arr[] = { 2, 3, 4, 5, 7 };
			int n = arr.length;
			int max_sum = maxSubArraySum(arr, 0, n - 1);

			System.out.println("Maximum contiguous sum is "
							+ max_sum);
		}
	}
